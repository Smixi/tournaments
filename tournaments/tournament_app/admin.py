from import_export.admin import ImportExportModelAdmin, admin
from .models import Game, Player, Phase, Match, Team, Tournament, TournamentTeam, Participant, Registration, Organizer, TeamRegistration

class GameResource(ImportExportModelAdmin):
    class Meta:
        model = Game
class PlayerResource(ImportExportModelAdmin):
    list_display = ('nickname',)
    class Meta:
        model = Player
class PhaseResource(ImportExportModelAdmin):
    class Meta:
        model = Phase
class MatchResource(ImportExportModelAdmin):
    class Meta:
        model = Match
class TeamResource(ImportExportModelAdmin):
    class Meta:
        model = Team
class TournamentResource(ImportExportModelAdmin):
    class Meta:
        model = Tournament
class TournamentTeamResource(ImportExportModelAdmin):
    class Meta:
        model = TournamentTeam
class ParticipantResource(ImportExportModelAdmin):
    class Meta:
        model = Participant
class OrganizerResource(ImportExportModelAdmin):
    class Meta:
        model = Organizer
class RegistrationResource(ImportExportModelAdmin):
    class Meta:
        model = Registration  
class TeamRegistrationResource(ImportExportModelAdmin):
    class Meta:
        model = TeamRegistration      

admin.site.register(Game, GameResource)
admin.site.register(Player, PlayerResource)
admin.site.register(Phase, PhaseResource)
admin.site.register(Match, MatchResource)
admin.site.register(Team, TeamResource)
admin.site.register(Tournament, TournamentResource)
admin.site.register(TournamentTeam, TournamentTeamResource)
admin.site.register(Participant, ParticipantResource)
admin.site.register(Organizer, OrganizerResource)
admin.site.register(Registration, RegistrationResource)
admin.site.register(TeamRegistration, TeamRegistrationResource)