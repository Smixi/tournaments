from django.db import models
from simple_history.models import HistoricalRecords

import rules
from rules.contrib.models import RulesModel 
from .organizer import Organizer
from .team import Team
from .tournament import Tournament
from .player import Player

class TeamRegistration(RulesModel):
    registration_info = models.TextField(blank=True)
    registration_date = models.DateTimeField(auto_now_add=True)
    team_display_name = models.CharField(max_length=100)

    team = models.ForeignKey(Team, on_delete=models.SET_NULL, null=True, blank=True)
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE, related_name="team_registrations")
    validated_by = models.ForeignKey(Organizer, on_delete=models.SET_NULL, null=True, blank=True)
    created_by = models.ForeignKey(Player, on_delete=models.SET_NULL, null=True, blank=True)

    history = HistoricalRecords()

    # Relations:
    # phase_set -> Foreign Key
    # participant_set -> Foreign Key
    # tournamentTeam_set -> Foreign Key
    class Meta:
        constraints = [
            models.UniqueConstraint(
                name='unique_team_tournament',
                fields=['team', 'tournament'],
            ),
            models.UniqueConstraint(
                name='unique_team_name_tournament',
                fields=['team_display_name', 'tournament'],
            ),
        ]
        db_table = "team_registration"

    def __str__(self):
        return f"Registration of {self.team_display_name} for tournament {self.tournament.name}"