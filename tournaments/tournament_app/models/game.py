from django.db import models
from enum import Enum
from simple_history.models import HistoricalRecords

class GameType(Enum):
    FPS = 'FPS'
    RACING = 'RACING'
    RTS = 'RTS'
    MOBA = 'MOBA'
    BATTLE_ROYAL = 'BR'
    OTHER = 'other'

    @classmethod
    def choices(cls):
        return [(key.value, key.name) for key in cls]


class Game(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(max_length=300, blank=True, null=True)
    genre = models.CharField(choices=GameType.choices(), default=GameType.OTHER, max_length=50)
    published_date = models.DateTimeField(null=True, blank=True)
    history = HistoricalRecords()
    class Meta:
        db_table = "game"
    
    def __str__(self):
        return self.name
