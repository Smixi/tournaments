from django.db import models
from django.contrib.auth.models import User
from django.db import models
from simple_history.models import HistoricalRecords
import rules
from rules.contrib.models import RulesModel

@rules.predicate
def is_user_player(user, player: 'Player'):
    return player.user == user

class Player(RulesModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, related_name="player")
    nickname = models.CharField(max_length=50, null=False, blank=False)
    history = HistoricalRecords()
    # Relations :
    games = models.ManyToManyField('Game', blank=True, db_table="players_like_games", related_name="played_by")
    # participant_set => from foreign key
    class Meta:
        db_table = "player"
        rules_permissions = {
            "add": rules.is_authenticated,
            "view": rules.always_true,
            "change": is_user_player,
            "delete": is_user_player
        }
    def __str__(self):
        return f"{self.nickname}"


# Add signal to create player when user is created.
from django.db.models.signals import post_save
def create_player_with_user(sender, instance, signal, *args, **kwargs):
    if sender is User:
        player = Player.objects.create(user=instance, nickname=instance.username)
        Player.save(player)

post_save.connect(create_player_with_user, sender=User)

