from .tournament import Tournament
from django.db import models
from django.contrib.auth.models import User
from django.db import models
from simple_history.models import HistoricalRecords

class Organizer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='organizer')
    history = HistoricalRecords()
    # Relations :

    # registration_set => Foreign Key

    class Meta:
        db_table = 'organizer'

    def __str__(self):
        return f"{self.user.username}"


# Add signal to create organizer when user is created.
from django.db.models.signals import post_save
def create_organizer_with_user(sender, instance, signal, *args, **kwargs):
    if sender is User:
        organizer = Organizer.objects.create(user=instance)
        Organizer.save(organizer)

post_save.connect(create_organizer_with_user, sender=User)