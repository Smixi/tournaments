from django.db import models
from simple_history.models import HistoricalRecords
from enum import Enum
import rules
from rules.contrib.models import RulesModel

def is_user_organizer(user, phase:'Phase'):
    return user.organizer in phase.tournament.organizers

class PhaseProgress(Enum):
    IN_PROGRESS = "in progress"
    DONE = "done"
    NOT_STARTED = "not started"

    @classmethod
    def choices(cls):
        return [(key.value, key.name) for key in cls]

class Phase(RulesModel):
    name = models.CharField(max_length=50)
    history = HistoricalRecords()
    progress = models.CharField(choices=PhaseProgress.choices(), default=PhaseProgress.NOT_STARTED, max_length=50)
    # Relations:
    playing_teams = models.ManyToManyField('TournamentTeam', blank=True, db_table="teams_qualified_phases")
    tournament = models.ForeignKey('Tournament', on_delete=models.CASCADE, related_name='phases')
    qualified_teams = models.ManyToManyField('TournamentTeam', blank=True, related_name="qualified_teams", db_table="teams_won_phases")
    # match_set => from foreign key
    class Meta:
        db_table = "phase"
        rules_permissions = {
            "add": is_user_organizer,
            "read": rules.always_true,
            "change": is_user_organizer,
            "destroy": is_user_organizer
        }
    def __str__(self):
        return f"{self.name} of tournament {self.tournament.name}"