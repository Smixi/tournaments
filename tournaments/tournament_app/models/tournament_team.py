from django.db.models.deletion import CASCADE
from django.db import models
from rules.predicates import predicate
from .team_registration import TeamRegistration
from simple_history.models import HistoricalRecords
import rules
from rules.contrib.models import RulesModel

@predicate
def is_user_organizer(user, tournament_team: 'TournamentTeam'):
    return user.organizer in tournament_team.registered_team.tournament.organizers

class TournamentTeam(RulesModel):
    history = HistoricalRecords()
    # Relations: 
    matches = models.ManyToManyField('Match', blank=True, db_table="matchs_compete_teams", related_name="tournament_teams")
    registered_team = models.OneToOneField(TeamRegistration, on_delete=models.CASCADE, primary_key=True)
    # registration -> ForeignKEy
    class Meta:
        db_table = "tournament_team"
        rules_permissions = {
            "add": is_user_organizer,
            "view": rules.always_true,
            "change": is_user_organizer,
            "destroy": is_user_organizer
        }
    
    def __str__(self):
        return f"{self.registered_team.team_display_name} for tournament {self.registered_team.tournament.name}"