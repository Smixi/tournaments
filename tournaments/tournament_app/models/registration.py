from django.db import models
from rules.predicates import predicate
from simple_history.models import HistoricalRecords
from .organizer import Organizer
from .player import Player
from .tournament import Tournament
from .tournament_team import TournamentTeam

import rules
from rules.contrib.models import RulesModel

@predicate
def is_user_organizer(user, registration: 'Registration'):
    return user.organizer in registration.tournament.organizers

@predicate
def is_user_owner(user, registration: 'Registration'):
    return user.organizer == registration.player.user


class Registration(RulesModel):
    registration_info = models.TextField(blank=True)
    registration_date = models.DateTimeField(auto_now_add=True)
    tournament_display_name = models.CharField(max_length=50)

    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE, related_name="registrations")
    tournament_team = models.ForeignKey(TournamentTeam, on_delete=models.CASCADE)
    validated_by = models.ForeignKey(Organizer, on_delete=models.CASCADE, null=True, blank=True)
    
    history = HistoricalRecords()

    # Relations:
    # phase_set -> Foreign Key
    # participant_set -> Foreign Key
    # tournamentTeam_set -> Foreign Key
    class Meta:
        constraints = [
            models.UniqueConstraint(
                name='unique_player_tournament',
                fields=['player', 'tournament'],
            ),
            models.UniqueConstraint(
                name='unique_player_name_tournament',
                fields=['tournament_display_name', 'tournament'],
            ),
        ]
        db_table = "registration"
        rules_permissions = {
            "add": rules.is_authenticated,
            "view": rules.always_true,
            "change": is_user_organizer,
            "destroy": is_user_organizer | is_user_owner,
            "validate": is_user_organizer
        }
    
    def __str__(self):
        return f"Registration of {self.player} for tournament {self.tournament.name} as {self.tournament_display_name}"