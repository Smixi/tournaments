from .registration import Registration
from django.db import models
from django.contrib.auth.models import User
from django.db import models
from simple_history.models import HistoricalRecords

class Participant(models.Model):
    history = HistoricalRecords()
    # Relations:
    registration = models.OneToOneField(Registration, on_delete=models.CASCADE, related_name="participant", primary_key=True)

    class Meta:
        db_table = "participant"
    
    def __str__(self):
        return f"Participation of {self.registration.player.nickname} as {self.registration.tournament_display_name} in tournament {self.registration.tournament.name}"
        