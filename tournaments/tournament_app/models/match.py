from .phase import Phase
from .tournament_team import TournamentTeam
from .tournament import Tournament
from django.db import models
from simple_history.models import HistoricalRecords
from . import Game
from enum import Enum

class MatchProgress(Enum):
    IN_PROGRESS = "in progress"
    DONE = "done"
    NOT_STARTED = "not started"

    @classmethod
    def choices(cls):
        return [(key.value, key.name) for key in cls]

class Match(models.Model):
    start_date = models.DateTimeField(blank=True, auto_now_add=True)
    end_date = models.DateTimeField(blank=True, null=True)
    progress = models.CharField(choices=MatchProgress.choices(), default=MatchProgress.NOT_STARTED, max_length=50)
    result = models.JSONField(null=True, blank=True) # Can be transformed to match result table later. Here for simplicity
    history = HistoricalRecords()

    # Relations:
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    phase = models.ForeignKey(Phase, on_delete=models.CASCADE, related_name="match")
    
    
    class Meta:
        constraints = [
            models.CheckConstraint(check=models.Q(end_date__gte=models.F('start_date')), name='start_date_gte_end_date_match')
        ]
        db_table = "match"
    
    def __str__(self):
        return f"Match {self.id} of phase {self.phase}"