from django.db.models import fields
from .phase import Phase
from django.db import models
from .tournament import Tournament
from simple_history.models import HistoricalRecords
from rules.contrib.models import RulesModel
import rules 


def user_owns_team(user, team: 'Team'):
    return user.player in team.owners

class Team(RulesModel):
    name = models.CharField(max_length=50, unique=True)
    history = HistoricalRecords()
    # Relations :
    players = models.ManyToManyField('Player', blank=True, db_table="members_of_teams", related_name="teams")
    owners = models.ManyToManyField('Player', blank=True, related_name='owned_teams', db_table="owners_of_teams")
    # tournamentTeams => Foreign key
    class Meta:
        db_table = "team"
        rules_permissions = {
            "add": rules.is_authenticated,
            "view": rules.always_true,
            "change": user_owns_team,
            "delete": user_owns_team
        }


    def __str__(self):
        return self.name

    