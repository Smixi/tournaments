from django.db import models
from rules.predicates import predicate
from simple_history.models import HistoricalRecords
import rules
from rules.contrib.models import RulesModel

@predicate
def is_user_organizer(user, tournament: 'Tournament'):
    return user.organizer in tournament.organizers

class Tournament(RulesModel):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    location = models.CharField(max_length=100, blank=True, null=True)
    history = HistoricalRecords()
    
    organizers = models.ManyToManyField('Organizer', blank=True, db_table="organizers_of_tournaments")

    # Relations:
    # phase_set -> Foreign Key
    # participant_set -> Foreign Key
    # tournamentTeam_set -> Foreign Key
    class Meta:
        constraints = [
            models.CheckConstraint(check=models.Q(end_date__gte=models.F('start_date')), name='start_date_gte_end_date_tournament')
        ]
        db_table = "tournament"
        rules_permissions = {
            "add": rules.is_authenticated,
            "view": rules.always_true,
            "change": is_user_organizer,
            "destroy": is_user_organizer
        }
    def __str__(self):
        return f"{self.name} at {self.location} - {self.start_date} to {self.end_date}"