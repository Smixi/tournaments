import rules
from rules.contrib.rest_framework import AutoPermissionViewSetMixin

class TeamRegistrationPermissionsViewMixin(AutoPermissionViewSetMixin):
    """Represent specific permissions for TeamRegistration"""
    permission_type_map = {
        **AutoPermissionViewSetMixin.permission_type_map,
        "validate": "validate"
        }