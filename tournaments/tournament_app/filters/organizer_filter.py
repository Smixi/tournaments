from django_filters import rest_framework as filters
from ..models import Organizer

class OrganizerFilter(filters.FilterSet):
    class Meta:
        model = Organizer
        fields = ['user', 'tournament']