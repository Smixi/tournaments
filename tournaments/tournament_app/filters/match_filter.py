from django_filters import rest_framework as filters
from ..models import Match

class MatchFilter(filters.FilterSet):
    class Meta:
        model = Match
        fields = ['start_date', 'end_date', 'progress', 'game', 'phase']