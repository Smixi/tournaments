from django_filters import rest_framework as filters
from ..models import TeamRegistration

class TeamRegistrationFilter(filters.FilterSet):
    class Meta:
        model = TeamRegistration
        fields = ['tournament']