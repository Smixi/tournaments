from django_filters import rest_framework as filters
from ..models import Tournament

class TournamentFilter(filters.FilterSet):
    class Meta:
        model = Tournament
        fields = ['name', 'start_date', 'end_date', 'location']