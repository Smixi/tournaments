from django_filters import rest_framework as filters
from ..models import TournamentTeam

class TournamentTeamFilter(filters.FilterSet):
    class Meta:
        model = TournamentTeam
        fields = ['registered_team__tournament']