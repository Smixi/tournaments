from django_filters import rest_framework as filters
from ..models import Registration

class RegistrationFilter(filters.FilterSet):
    class Meta:
        model = Registration
        fields = ['tournament', 'tournament_team', 'validated_by', 'player']