from django_filters import rest_framework as filters
from ..models import Phase

class PhaseFilter(filters.FilterSet):
    class Meta:
        model = Phase
        fields = ['name', 'tournament']