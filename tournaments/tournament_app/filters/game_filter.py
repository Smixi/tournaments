from django_filters import rest_framework as filters
from ..models import Game

class GameFilter(filters.FilterSet):
    class Meta:
        model = Game
        fields = ['name', 'genre', 'played_by']