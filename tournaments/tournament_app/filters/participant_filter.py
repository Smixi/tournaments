from django_filters import rest_framework as filters
from ..models import Participant

class ParticipantFilter(filters.FilterSet):
    class Meta:
        model = Participant
        fields = []