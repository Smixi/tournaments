from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter
from .views import GameViewSet, PlayerViewSet, TeamViewSet, MatchViewSet, TournamentViewSet, PhaseViewSet, RegistrationViewSet, \
    TeamRegistrationViewSet, TournamentTeamViewSet, ParticipantViewSet 

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'games', GameViewSet)
router.register(r'players', PlayerViewSet)
router.register(r'matchs', MatchViewSet)
router.register(r'teams', TeamViewSet)
router.register(r'tournaments', TournamentViewSet)
router.register(r'phases', PhaseViewSet)
router.register(r'registrations', RegistrationViewSet)
router.register(r'team-registrations', TeamRegistrationViewSet)
router.register(r'tournament-teams', TournamentTeamViewSet)
router.register(r'participants', ParticipantViewSet)
# The API URLs are now determined automatically by the router.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^auth/', include('djoser.urls.jwt')),
]