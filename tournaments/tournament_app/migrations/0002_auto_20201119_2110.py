# Generated by Django 3.1.3 on 2020-11-19 21:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tournament_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='participant',
            table='participant',
        ),
    ]
