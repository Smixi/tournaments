# Generated by Django 3.1.5 on 2021-01-28 01:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tournament_app', '0002_auto_20210118_0129'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalmatch',
            name='end_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='historicalmatch',
            name='start_date',
            field=models.DateTimeField(blank=True, editable=False),
        ),
        migrations.AlterField(
            model_name='match',
            name='end_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='match',
            name='phase',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='match', to='tournament_app.phase'),
        ),
        migrations.AlterField(
            model_name='match',
            name='start_date',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='participant',
            name='registration',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='participant', to='tournament_app.registration'),
        ),
        migrations.AlterField(
            model_name='phase',
            name='tournament',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='phases', to='tournament_app.tournament'),
        ),
        migrations.AlterField(
            model_name='registration',
            name='tournament',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='registrations', to='tournament_app.tournament'),
        ),
        migrations.AlterField(
            model_name='teamregistration',
            name='tournament',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='team_registrations', to='tournament_app.tournament'),
        ),
    ]
