# Generated by Django 3.1.3 on 2020-12-20 19:02

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.db.models.expressions
import simple_history.models
import tournament_app.models.game
import tournament_app.models.match
import tournament_app.models.phase


class Migration(migrations.Migration):

    replaces = [('tournament_app', '0001_initial'), ('tournament_app', '0002_auto_20201119_2110'), ('tournament_app', '0003_auto_20201213_2252'), ('tournament_app', '0004_auto_20201213_2303'), ('tournament_app', '0005_auto_20201213_2356'), ('tournament_app', '0006_auto_20201214_0007'), ('tournament_app', '0007_auto_20201214_0016'), ('tournament_app', '0008_auto_20201214_0036')]

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(blank=True, max_length=300, null=True)),
                ('genre', models.CharField(choices=[('FPS', 'FPS'), ('RACING', 'RACING'), ('RTS', 'RTS'), ('MOBA', 'MOBA'), ('BR', 'BATTLE_ROYAL'), ('other', 'OTHER')], default=tournament_app.models.game.GameType['OTHER'], max_length=50)),
                ('published_date', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'game',
            },
        ),
        migrations.CreateModel(
            name='HistoricalGame',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(blank=True, max_length=300, null=True)),
                ('genre', models.CharField(choices=[('FPS', 'FPS'), ('RACING', 'RACING'), ('RTS', 'RTS'), ('MOBA', 'MOBA'), ('BR', 'BATTLE_ROYAL'), ('other', 'OTHER')], default=tournament_app.models.game.GameType['OTHER'], max_length=50)),
                ('published_date', models.DateTimeField(blank=True, null=True)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical game',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalMatch',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('start_date', models.DateTimeField(blank=True)),
                ('end_date', models.DateTimeField(blank=True)),
                ('progress', models.CharField(choices=[('in progress', 'IN_PROGRESS'), ('done', 'DONE'), ('not started', 'NOT_STARTED')], default=tournament_app.models.match.MatchProgress['NOT_STARTED'], max_length=50)),
                ('result', models.JSONField()),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical match',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalParticipant',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('participant_display_name', models.CharField(db_index=True, max_length=100)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical participant',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalPhase',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('progress', models.CharField(choices=[('in progress', 'IN_PROGRESS'), ('done', 'DONE'), ('not started', 'NOT_STARTED')], default=tournament_app.models.phase.PhaseProgress['NOT_STARTED'], max_length=50)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical phase',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalPlayer',
            fields=[
                ('nickname', models.CharField(max_length=50)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical player',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalTeam',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=50)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical team',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalTournament',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(blank=True)),
                ('start_date', models.DateTimeField(blank=True, null=True)),
                ('end_date', models.DateTimeField(blank=True, null=True)),
                ('location', models.CharField(blank=True, max_length=100, null=True)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical tournament',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalTournamentTeam',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('tournament_display_name', models.CharField(db_index=True, max_length=80)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical tournament team',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='Match',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_date', models.DateTimeField(blank=True)),
                ('end_date', models.DateTimeField(blank=True)),
                ('progress', models.CharField(choices=[('in progress', 'IN_PROGRESS'), ('done', 'DONE'), ('not started', 'NOT_STARTED')], default=tournament_app.models.match.MatchProgress['NOT_STARTED'], max_length=50)),
                ('result', models.JSONField()),
            ],
            options={
                'db_table': 'match',
            },
        ),
        migrations.CreateModel(
            name='Participant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('participant_display_name', models.CharField(max_length=100, unique=True)),
            ],
            options={
                'db_table': 'Participant',
            },
        ),
        migrations.CreateModel(
            name='Phase',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('progress', models.CharField(choices=[('in progress', 'IN_PROGRESS'), ('done', 'DONE'), ('not started', 'NOT_STARTED')], default=tournament_app.models.phase.PhaseProgress['NOT_STARTED'], max_length=50)),
            ],
            options={
                'db_table': 'phase',
            },
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='auth.user')),
                ('nickname', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'player',
            },
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True)),
            ],
            options={
                'db_table': 'team',
            },
        ),
        migrations.CreateModel(
            name='Tournament',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(blank=True)),
                ('start_date', models.DateTimeField(blank=True, null=True)),
                ('end_date', models.DateTimeField(blank=True, null=True)),
                ('location', models.CharField(blank=True, max_length=100, null=True)),
            ],
            options={
                'db_table': 'tournament',
            },
        ),
        migrations.CreateModel(
            name='TournamentTeam',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tournament_display_name', models.CharField(max_length=80, unique=True)),
                ('matches', models.ManyToManyField(blank=True, db_table='matchs_compete_teams', to='tournament_app.Match')),
                ('team', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament_app.team')),
                ('tournament', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='tournament_app.tournament')),
            ],
            options={
                'db_table': 'tournament_team',
            },
        ),
        migrations.AddConstraint(
            model_name='tournament',
            constraint=models.CheckConstraint(check=models.Q(end_date__gte=django.db.models.expressions.F('start_date')), name='start_date_gte_end_date_tournament'),
        ),
        migrations.AddField(
            model_name='team',
            name='owners',
            field=models.ManyToManyField(blank=True, db_table='owners_of_teams', related_name='owners', to='tournament_app.Player'),
        ),
        migrations.AddField(
            model_name='team',
            name='players',
            field=models.ManyToManyField(blank=True, db_table='members_of_teams', to='tournament_app.Player'),
        ),
        migrations.AddField(
            model_name='player',
            name='games',
            field=models.ManyToManyField(blank=True, db_table='players_like_games', to='tournament_app.Game'),
        ),
        migrations.AddField(
            model_name='phase',
            name='playing_teams',
            field=models.ManyToManyField(blank=True, db_table='teams_qualified_phases', to='tournament_app.TournamentTeam'),
        ),
        migrations.AddField(
            model_name='phase',
            name='qualified_teams',
            field=models.ManyToManyField(blank=True, db_table='teams_won_phases', related_name='qualified_teams', to='tournament_app.TournamentTeam'),
        ),
        migrations.AddField(
            model_name='phase',
            name='tournament',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament_app.tournament'),
        ),
        migrations.AddField(
            model_name='participant',
            name='player',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament_app.player'),
        ),
        migrations.AddField(
            model_name='participant',
            name='tournament',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament_app.tournament'),
        ),
        migrations.AddField(
            model_name='participant',
            name='tournament_team',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament_app.tournamentteam'),
        ),
        migrations.AddField(
            model_name='match',
            name='game',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament_app.game'),
        ),
        migrations.AddField(
            model_name='match',
            name='phase',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament_app.phase'),
        ),
        migrations.AddField(
            model_name='historicaltournamentteam',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicaltournamentteam',
            name='team',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.team'),
        ),
        migrations.AddField(
            model_name='historicaltournamentteam',
            name='tournament',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.tournament'),
        ),
        migrations.AddField(
            model_name='historicaltournament',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalteam',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalplayer',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalplayer',
            name='user',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalphase',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalphase',
            name='tournament',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.tournament'),
        ),
        migrations.AddField(
            model_name='historicalparticipant',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalparticipant',
            name='player',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.player'),
        ),
        migrations.AddField(
            model_name='historicalparticipant',
            name='tournament',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.tournament'),
        ),
        migrations.AddField(
            model_name='historicalparticipant',
            name='tournament_team',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.tournamentteam'),
        ),
        migrations.AddField(
            model_name='historicalmatch',
            name='game',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.game'),
        ),
        migrations.AddField(
            model_name='historicalmatch',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalmatch',
            name='phase',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.phase'),
        ),
        migrations.AddField(
            model_name='historicalgame',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddConstraint(
            model_name='participant',
            constraint=models.UniqueConstraint(fields=('player', 'tournament'), name='unique_player_tournament'),
        ),
        migrations.AddConstraint(
            model_name='match',
            constraint=models.CheckConstraint(check=models.Q(end_date__gte=django.db.models.expressions.F('start_date')), name='start_date_gte_end_date_match'),
        ),
        migrations.AlterModelTable(
            name='participant',
            table='participant',
        ),
        migrations.CreateModel(
            name='HistoricalOrganizer',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical organizer',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalRegistration',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(blank=True)),
                ('registration_date', models.DateTimeField()),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical registration',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='Organizer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
            options={
                'db_table': 'organizer',
            },
        ),
        migrations.CreateModel(
            name='Registration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(blank=True)),
                ('registration_date', models.DateTimeField()),
            ],
            options={
                'db_table': 'registration',
            },
        ),
        migrations.RemoveConstraint(
            model_name='participant',
            name='unique_player_tournament',
        ),
        migrations.RemoveField(
            model_name='historicalparticipant',
            name='player',
        ),
        migrations.RemoveField(
            model_name='historicalparticipant',
            name='tournament',
        ),
        migrations.RemoveField(
            model_name='historicalparticipant',
            name='tournament_team',
        ),
        migrations.RemoveField(
            model_name='participant',
            name='player',
        ),
        migrations.RemoveField(
            model_name='participant',
            name='tournament',
        ),
        migrations.RemoveField(
            model_name='participant',
            name='tournament_team',
        ),
        migrations.AddField(
            model_name='registration',
            name='player',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament_app.player'),
        ),
        migrations.AddField(
            model_name='registration',
            name='tournament',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament_app.tournament'),
        ),
        migrations.AddField(
            model_name='registration',
            name='tournament_team',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament_app.tournamentteam'),
        ),
        migrations.AddField(
            model_name='registration',
            name='validated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='tournament_app.organizer'),
        ),
        migrations.AddField(
            model_name='organizer',
            name='tournament',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament_app.tournament'),
        ),
        migrations.AddField(
            model_name='organizer',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalregistration',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalregistration',
            name='player',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.player'),
        ),
        migrations.AddField(
            model_name='historicalregistration',
            name='tournament',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.tournament'),
        ),
        migrations.AddField(
            model_name='historicalregistration',
            name='tournament_team',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.tournamentteam'),
        ),
        migrations.AddField(
            model_name='historicalregistration',
            name='validated_by',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.organizer'),
        ),
        migrations.AddField(
            model_name='historicalorganizer',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalorganizer',
            name='tournament',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.tournament'),
        ),
        migrations.AddField(
            model_name='historicalorganizer',
            name='user',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalparticipant',
            name='registration',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.registration'),
        ),
        migrations.AddField(
            model_name='participant',
            name='registration',
            field=models.OneToOneField(default=None, on_delete=django.db.models.deletion.CASCADE, to='tournament_app.registration'),
            preserve_default=False,
        ),
        migrations.AddConstraint(
            model_name='registration',
            constraint=models.UniqueConstraint(fields=('player', 'tournament'), name='unique_player_tournament'),
        ),
        migrations.AddConstraint(
            model_name='organizer',
            constraint=models.UniqueConstraint(fields=('user', 'tournament'), name='unique_user_tournament'),
        ),
        migrations.RenameField(
            model_name='historicalregistration',
            old_name='description',
            new_name='registration_info',
        ),
        migrations.RenameField(
            model_name='registration',
            old_name='description',
            new_name='registration_info',
        ),
        migrations.RemoveField(
            model_name='historicalregistration',
            name='name',
        ),
        migrations.RemoveField(
            model_name='registration',
            name='name',
        ),
        migrations.AlterField(
            model_name='organizer',
            name='tournament',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='tournament_app.tournament'),
        ),
        migrations.RemoveField(
            model_name='historicaltournamentteam',
            name='team',
        ),
        migrations.RemoveField(
            model_name='tournamentteam',
            name='team',
        ),
        migrations.CreateModel(
            name='TeamRegistration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('registration_info', models.TextField(blank=True)),
                ('registration_date', models.DateTimeField()),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='tournament_app.player')),
                ('team', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='tournament_app.team')),
                ('tournament', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament_app.tournament')),
                ('validated_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='tournament_app.organizer')),
            ],
            options={
                'db_table': 'team_registration',
            },
        ),
        migrations.CreateModel(
            name='HistoricalTeamRegistration',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('registration_info', models.TextField(blank=True)),
                ('registration_date', models.DateTimeField()),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('created_by', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.player')),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('team', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.team')),
                ('tournament', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.tournament')),
                ('validated_by', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.organizer')),
            ],
            options={
                'verbose_name': 'historical team registration',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.AddField(
            model_name='historicaltournamentteam',
            name='registered_team',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.teamregistration'),
        ),
        migrations.AddField(
            model_name='tournamentteam',
            name='registered_team',
            field=models.OneToOneField(default=None, on_delete=django.db.models.deletion.CASCADE, to='tournament_app.teamregistration'),
            preserve_default=False,
        ),
        migrations.AddConstraint(
            model_name='teamregistration',
            constraint=models.UniqueConstraint(fields=('team', 'tournament'), name='unique_team_tournament'),
        ),
        migrations.RemoveField(
            model_name='historicalparticipant',
            name='participant_display_name',
        ),
        migrations.RemoveField(
            model_name='historicaltournamentteam',
            name='tournament',
        ),
        migrations.RemoveField(
            model_name='historicaltournamentteam',
            name='tournament_display_name',
        ),
        migrations.RemoveField(
            model_name='participant',
            name='participant_display_name',
        ),
        migrations.RemoveField(
            model_name='tournamentteam',
            name='tournament',
        ),
        migrations.RemoveField(
            model_name='tournamentteam',
            name='tournament_display_name',
        ),
        migrations.AddField(
            model_name='historicalregistration',
            name='tournament_display_name',
            field=models.CharField(default=None, max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='historicalteamregistration',
            name='team_display_name',
            field=models.CharField(default=None, max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='registration',
            name='tournament_display_name',
            field=models.CharField(default=None, max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='teamregistration',
            name='team_display_name',
            field=models.CharField(default=None, max_length=100),
            preserve_default=False,
        ),
        migrations.AddConstraint(
            model_name='registration',
            constraint=models.UniqueConstraint(fields=('tournament_display_name', 'tournament'), name='unique_player_name_tournament'),
        ),
        migrations.AddConstraint(
            model_name='teamregistration',
            constraint=models.UniqueConstraint(fields=('team_display_name', 'tournament'), name='unique_team_name_tournament'),
        ),
        migrations.RemoveConstraint(
            model_name='organizer',
            name='unique_user_tournament',
        ),
        migrations.RemoveField(
            model_name='historicalorganizer',
            name='tournament',
        ),
        migrations.RemoveField(
            model_name='organizer',
            name='tournament',
        ),
        migrations.AddField(
            model_name='tournament',
            name='organizers',
            field=models.ManyToManyField(blank=True, db_table='organizers_of_tournaments', to='tournament_app.Organizer'),
        ),
        migrations.AlterField(
            model_name='historicalmatch',
            name='result',
            field=models.JSONField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='match',
            name='result',
            field=models.JSONField(blank=True, null=True),
        ),
    ]
