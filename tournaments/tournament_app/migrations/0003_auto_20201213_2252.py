# Generated by Django 3.1.3 on 2020-12-13 22:52

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import simple_history.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('tournament_app', '0002_auto_20201119_2110'),
    ]

    operations = [
        migrations.CreateModel(
            name='HistoricalOrganizer',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical organizer',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalRegistration',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(blank=True)),
                ('registration_date', models.DateTimeField()),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical registration',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='Organizer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
            options={
                'db_table': 'organizer',
            },
        ),
        migrations.CreateModel(
            name='Registration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(blank=True)),
                ('registration_date', models.DateTimeField()),
            ],
            options={
                'db_table': 'registration',
            },
        ),
        migrations.RemoveConstraint(
            model_name='participant',
            name='unique_player_tournament',
        ),
        migrations.RemoveField(
            model_name='historicalparticipant',
            name='player',
        ),
        migrations.RemoveField(
            model_name='historicalparticipant',
            name='tournament',
        ),
        migrations.RemoveField(
            model_name='historicalparticipant',
            name='tournament_team',
        ),
        migrations.RemoveField(
            model_name='participant',
            name='player',
        ),
        migrations.RemoveField(
            model_name='participant',
            name='tournament',
        ),
        migrations.RemoveField(
            model_name='participant',
            name='tournament_team',
        ),
        migrations.AddField(
            model_name='registration',
            name='player',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament_app.player'),
        ),
        migrations.AddField(
            model_name='registration',
            name='tournament',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament_app.tournament'),
        ),
        migrations.AddField(
            model_name='registration',
            name='tournament_team',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament_app.tournamentteam'),
        ),
        migrations.AddField(
            model_name='registration',
            name='validated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='tournament_app.organizer'),
        ),
        migrations.AddField(
            model_name='organizer',
            name='tournament',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament_app.tournament'),
        ),
        migrations.AddField(
            model_name='organizer',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalregistration',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalregistration',
            name='player',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.player'),
        ),
        migrations.AddField(
            model_name='historicalregistration',
            name='tournament',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.tournament'),
        ),
        migrations.AddField(
            model_name='historicalregistration',
            name='tournament_team',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.tournamentteam'),
        ),
        migrations.AddField(
            model_name='historicalregistration',
            name='validated_by',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.organizer'),
        ),
        migrations.AddField(
            model_name='historicalorganizer',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalorganizer',
            name='tournament',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.tournament'),
        ),
        migrations.AddField(
            model_name='historicalorganizer',
            name='user',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalparticipant',
            name='registration',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='tournament_app.registration'),
        ),
        migrations.AddField(
            model_name='participant',
            name='registration',
            field=models.OneToOneField(default=None, on_delete=django.db.models.deletion.CASCADE, to='tournament_app.registration'),
            preserve_default=False,
        ),
        migrations.AddConstraint(
            model_name='registration',
            constraint=models.UniqueConstraint(fields=('player', 'tournament'), name='unique_player_tournament'),
        ),
        migrations.AddConstraint(
            model_name='organizer',
            constraint=models.UniqueConstraint(fields=('user', 'tournament'), name='unique_user_tournament'),
        ),
    ]
