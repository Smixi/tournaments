from django_filters import rest_framework as filters
from rest_framework import viewsets
from ..models import Tournament
from ..serializer import TournamentSerializer, TournamentInputSerializer, TournamentDetailsSerializer
from ..filters import TournamentFilter
from rules.contrib.rest_framework import AutoPermissionViewSetMixin

class TournamentViewSet(AutoPermissionViewSetMixin, viewsets.ModelViewSet):
    queryset = Tournament.objects.all()
    serializer_class = TournamentSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = TournamentFilter

    def get_serializer_class(self):
        if self.action == 'create':
            return TournamentInputSerializer
        if self.action == 'retrieve':
            return TournamentDetailsSerializer
        return TournamentViewSet.serializer_class