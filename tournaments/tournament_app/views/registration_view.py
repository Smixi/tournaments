from ..serializer.empty_serializer import EmptySerializer
from django_filters import rest_framework as filters
from rest_framework import viewsets
from ..models import Registration, Participant
from ..serializer import RegistrationSerializer, RegistrationInputSerializer
from ..filters import RegistrationFilter
from ..permissions import TeamRegistrationPermissionsViewMixin
from rest_framework.decorators import action
from rest_framework.response import Response
from drf_spectacular.utils import extend_schema

class RegistrationViewSet(TeamRegistrationPermissionsViewMixin, viewsets.ModelViewSet):
    queryset = Registration.objects.all()
    serializer_class = RegistrationSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = RegistrationFilter

    @extend_schema(responses=RegistrationSerializer)
    @action(methods=['post'], detail=True)
    def validate(self, request, pk=None):
        registration: Registration = self.get_object()
        registration.validated_by = request.user.organizer
        Participant.objects.create(registration=registration)
        Registration.save(registration)
        serializer = RegistrationSerializer(registration)
        return Response(serializer.data, status=204)

    def get_serializer_class(self):
        if self.action == 'create':
            return RegistrationInputSerializer
        if self.action == 'validate':
            return EmptySerializer
        return self.serializer_class