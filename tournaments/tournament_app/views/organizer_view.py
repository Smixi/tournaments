from django_filters import rest_framework as filters
from rest_framework import viewsets
from ..models import Organizer
from ..serializer import OrganizerSerializer
from ..filters import OrganizerFilter

class OrganizerViewSet(viewsets.ModelViewSet):
    queryset = Organizer.objects.all()
    serializer_class = OrganizerSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = OrganizerFilter