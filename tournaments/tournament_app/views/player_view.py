from rest_framework import viewsets
from ..models import Player
from ..serializer import PlayerSerializer, PlayerInputSerializer
from rest_framework.decorators import action
from rules.contrib.rest_framework import AutoPermissionViewSetMixin
from drf_spectacular.utils import extend_schema

class PlayerViewSet(AutoPermissionViewSetMixin, viewsets.ModelViewSet):
    queryset = Player.objects.all()
    serializer_class = PlayerSerializer

    @extend_schema(responses=PlayerSerializer)
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    def get_serializer_class(self):
        if self.action == 'create' or self.action == 'update':
            return PlayerInputSerializer
        return PlayerViewSet.serializer_class