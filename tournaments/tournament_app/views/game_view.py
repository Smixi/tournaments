from django_filters import rest_framework as filters
from rest_framework import viewsets
from ..models import Game
from ..serializer import GameSerializer
from ..filters import GameFilter

class GameViewSet(viewsets.ModelViewSet):
    queryset = Game.objects.all()
    serializer_class = GameSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = GameFilter