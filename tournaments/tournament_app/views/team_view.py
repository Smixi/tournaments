from django_filters import rest_framework as filters
from rest_framework import viewsets
from ..models import Team
from ..serializer import TeamSerializer, TeamInputSerializer
from ..filters import TeamFilter

class TeamViewSet(viewsets.ModelViewSet):
    queryset = Team.objects.all()
    serializer_class = TeamSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = TeamFilter

    def get_serializer_class(self):
        if self.action == 'create':
            return TeamInputSerializer
        return TeamViewSet.serializer_class