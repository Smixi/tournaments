from ..serializer.empty_serializer import EmptySerializer
from django_filters import rest_framework as filters
from rest_framework import viewsets
from ..models import TeamRegistration, TournamentTeam
from ..serializer import TeamRegistrationInputSerializer, TeamRegistrationSerializer
from ..filters import TeamRegistrationFilter
from rest_framework.decorators import action
from rest_framework.response import Response
from drf_spectacular.utils import extend_schema 

class TeamRegistrationViewSet(viewsets.ModelViewSet):
    queryset = TeamRegistration.objects.all()
    serializer_class = TeamRegistrationSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = TeamRegistrationFilter

    @extend_schema(responses=TeamRegistrationSerializer)
    @action(methods=['post'], detail=True)
    def validate(self, request, pk=None):
        team_registration: TeamRegistration = self.get_object()
        team_registration.validated_by = request.user.organizer
        TournamentTeam.objects.create(registered_team=team_registration)
        TeamRegistration.save(team_registration)
        serializer = TeamRegistrationSerializer(team_registration)
        return Response(serializer.data,status=204)

    def get_serializer_class(self):
        if self.action == 'create':
            return TeamRegistrationInputSerializer
        if self.action == 'validate':
            return EmptySerializer
        return self.serializer_class