from django_filters import rest_framework as filters
from rest_framework import viewsets
from ..models import Match
from ..serializer import MatchSerializer, MatchInputSerializer
from ..filters import MatchFilter

class MatchViewSet(viewsets.ModelViewSet):
    queryset = Match.objects.all()
    serializer_class = MatchSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = MatchFilter
    
    def get_serializer_class(self):
        if self.action == 'create':
            return MatchInputSerializer
        return MatchViewSet.serializer_class