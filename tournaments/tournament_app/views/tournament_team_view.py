from django_filters import rest_framework as filters
from rest_framework import viewsets
from ..models import TournamentTeam
from ..serializer import TournamentTeamSerializer
from ..filters import TournamentTeamFilter

class TournamentTeamViewSet(viewsets.ModelViewSet):
    queryset = TournamentTeam.objects.all()
    serializer_class = TournamentTeamSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = TournamentTeamFilter