from django_filters import rest_framework as filters
from rest_framework import viewsets
from ..models import Participant
from ..serializer import ParticipantSerializer
from ..filters import ParticipantFilter

class ParticipantViewSet(viewsets.ModelViewSet):
    queryset = Participant.objects.all()
    serializer_class = ParticipantSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ParticipantFilter