from django_filters import rest_framework as filters
from drf_spectacular.utils import extend_schema
from rest_framework import viewsets
from rest_framework.response import Response
from ..models import Phase, TournamentTeam
from ..serializer import PhaseSerializer, PhaseInputSerializer
from ..filters import PhaseFilter
from rest_framework.decorators import action
from rest_framework.serializers import ModelSerializer


class PhaseTeamPlayingSerializer(ModelSerializer):
    class Meta:
        model = Phase
        fields = ['playing_teams']

class PhaseTeamQualifiedSerializer(ModelSerializer):
    class Meta:
        model = Phase
        fields = ['qualified_teams']

class PhaseViewSet(viewsets.ModelViewSet):
    queryset = Phase.objects.all()
    serializer_class = PhaseSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = PhaseFilter

    @extend_schema(request=PhaseTeamPlayingSerializer, responses=PhaseSerializer)
    @action(methods=['post'], detail=True)
    def add_team_playing(self, request, pk=None):
        serializer = PhaseTeamPlayingSerializer(data=request.data)
        serializer.is_valid()
        phase: Phase = self.get_object()
        phase.playing_teams.add(*serializer.validated_data['playing_teams'])
        phase.save()
        output_serializer = PhaseSerializer(phase)
        return Response(output_serializer.data, 204)

    @extend_schema(request=PhaseTeamQualifiedSerializer, responses=PhaseSerializer)
    @action(methods=['post'], detail=True)
    def add_team_playing_qualified(self, request, pk=None):
        serializer = PhaseTeamQualifiedSerializer(data=request.data)
        serializer.is_valid()
        phase: Phase = self.get_object()
        phase.qualified_teams.add(*serializer.validated_data['qualified_teams'])
        phase.save()
        output_serializer = PhaseSerializer(phase)
        return Response(output_serializer.data, 204)

    def get_serializer_class(self):
        if self.action == 'create':
            return PhaseInputSerializer
        return PhaseViewSet.serializer_class