from rest_framework import serializers
from ..models import TeamRegistration
from .team_serializer import TeamSerializer 
class TeamRegistrationSerializer(serializers.ModelSerializer):
    team = TeamSerializer(read_only=True)
    class Meta:
        model = TeamRegistration
        fields = ['id', 'registration_info', 'registration_date', 'team_display_name', 'tournament', 'team', 'validated_by']

class TeamRegistrationInputSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeamRegistration
        fields = ['registration_info', 'team', 'tournament', 'team_display_name']