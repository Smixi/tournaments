
from rest_framework import serializers
from ..models import Match
from .tournament_team_serializer import TournamentTeamSerializer
from .game_serializer import GameSerializer
class MatchSerializer(serializers.ModelSerializer):
    game = GameSerializer(read_only=True)
    tournament_teams = TournamentTeamSerializer(many=True, read_only=True)
    class Meta:
        model = Match
        fields = ['id', 'start_date', 'end_date', 'progress', 'game', 'phase', 'tournament_teams', 'result']
class MatchInputSerializer(serializers.ModelSerializer):
    class Meta:
        model = Match
        fields = ['start_date', 'end_date', 'game', 'phase', 'tournament_teams', 'result']