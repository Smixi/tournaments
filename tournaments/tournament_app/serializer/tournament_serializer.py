from rest_framework import serializers
from ..models import Tournament 
from .phase_serializer import PhaseSerializer
class TournamentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tournament
        fields = ['id', 'name', 'description', 'start_date', 'end_date', 'location', 'organizers']


class TournamentInputSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        organizer = self.context['request'].user.organizer
        tournament = super().create(validated_data)
        tournament.organizers.add(organizer)
        return tournament
    class Meta:
        model = Tournament
        fields = ['name', 'description', 'start_date', 'end_date', 'location', 'organizers']

class TournamentDetailsSerializer(serializers.ModelSerializer):
    phases = PhaseSerializer(many=True)
    class Meta:
        model = Tournament
        fields = ['id', 'name', 'description', 'start_date', 'end_date', 'location', 'organizers', 
                  'phases', 'team_registrations', 'registrations']
        depth = 1