from rest_framework import serializers
from .team_registration_serializer import TeamRegistrationSerializer
from ..models import TournamentTeam 
class TournamentTeamSerializer(serializers.ModelSerializer):
    registered_team = TeamRegistrationSerializer()
    class Meta:
        model = TournamentTeam
        fields = ['matches', 'registered_team']