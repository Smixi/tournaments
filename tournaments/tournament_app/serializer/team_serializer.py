from rest_framework import serializers
from ..models import Team
class TeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = ['id', 'name', 'players']

class TeamInputSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        player = self.context['request'].user.player
        team = super().create(validated_data)
        team.owners.add(player)
        return team
    class Meta:
        model = Team
        fields = ['name', 'players']