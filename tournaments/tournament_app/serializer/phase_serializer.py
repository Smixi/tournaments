from django.db.models import fields
from rest_framework import serializers
from ..models import Phase
from .tournament_team_serializer import TournamentTeamSerializer
class PhaseSerializer(serializers.ModelSerializer):
    playing_teams = TournamentTeamSerializer(many=True)
    qualified_teams = TournamentTeamSerializer(many=True)
    class Meta:
        model = Phase
        fields = ['id', 'qualified_teams', 'tournament', 'name', 'playing_teams', 'match', 'progress']
class PhaseInputSerializer(serializers.ModelSerializer):
    class Meta:
        model = Phase
        fields = ['tournament', 'name', 'playing_teams']