from rest_framework import serializers
from ..models import Participant
from .registration_serializer import RegistrationSerializer 
class ParticipantSerializer(serializers.ModelSerializer):
    registration = RegistrationSerializer()
    class Meta:
        model = Participant
        fields = ['registration']