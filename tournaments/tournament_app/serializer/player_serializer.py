from rest_framework import serializers
from ..models import Player, Game

class PlayerSerializer(serializers.ModelSerializer):
    games = serializers.SlugRelatedField(many=True, slug_field='name', read_only=True)
    class Meta:
        model = Player
        fields = ['user', 'games', 'teams', 'nickname']

class PlayerInputSerializer(serializers.ModelSerializer):
    class Meta:
        model = Player
        fields = ['games', 'nickname']