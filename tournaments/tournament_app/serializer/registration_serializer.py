from rest_framework import serializers
from ..models import Registration
from .tournament_team_serializer import TournamentTeamSerializer
class RegistrationSerializer(serializers.ModelSerializer):
    tournament_team = TournamentTeamSerializer()
    class Meta:
        model = Registration
        depth = 1
        fields = ['id', 'registration_info', 'tournament_display_name', 'registration_date', 'player', 'tournament', 'tournament_team', 'validated_by']

class RegistrationInputSerializer(serializers.ModelSerializer):
    class Meta:
        model = Registration
        fields = ['registration_info', 'tournament_display_name', 'player', 'tournament', 'tournament_team']